package chatservidor;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;

/**
 * @author LuisAngelCuevasObrador
 */
public class Servidor extends Thread{    
    public  ServerSocket serverSocket;
    LinkedList<HiloCliente> clientes;
    
    public  final PanelServidor ventana;
    public  final String puerto;
    public static int correlativo;
    public Servidor(String puerto, PanelServidor ventana) {
        correlativo=0;
        this.puerto=puerto;
        this.ventana=ventana;
        clientes=new LinkedList<>();
        this.start();
    }
    public void run() {
        try {
            serverSocket = new ServerSocket(Integer.valueOf(puerto));
            ventana.addServidorIniciado();
            while (true) {
                HiloCliente h;
                Socket socket;
                socket = serverSocket.accept();
                System.out.println("Nueva conexion entrante: "+socket);
                h=new HiloCliente(socket, this);               
                h.start();
            }
        } catch (Exception e) {
            System.out.println(e);
            System.exit(0);
        }                
    }   
    
    LinkedList<String> getUsuariosConectados() {
        LinkedList<String>usuariosConectados=new LinkedList<>();
        clientes.stream().forEach(c -> usuariosConectados.add(c.getIdentificador()));
        return usuariosConectados;
    }
    
    void agregarRegistro(String texto) {
        ventana.agregarLog(texto);
    }
}
